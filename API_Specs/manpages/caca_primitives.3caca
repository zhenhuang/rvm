.TH "caca_primitives" 3caca "Sun Feb 7 2016" "Version 0.99.beta19" "libcaca" \" -*- nroff -*-
.ad l
.nh
.SH NAME
caca_primitives \- libcaca primitives drawing
.SS "Functions"

.in +1c
.ti -1c
.RI "__extern int \fBcaca_draw_line\fP (\fBcaca_canvas_t\fP *, int, int, int, int, uint32_t)"
.br
.RI "\fIDraw a line on the canvas using the given character\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_draw_polyline\fP (\fBcaca_canvas_t\fP *, int const x[], int const y[], int, uint32_t)"
.br
.RI "\fIDraw a polyline\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_draw_thin_line\fP (\fBcaca_canvas_t\fP *, int, int, int, int)"
.br
.RI "\fIDraw a thin line on the canvas, using ASCII art\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_draw_thin_polyline\fP (\fBcaca_canvas_t\fP *, int const x[], int const y[], int)"
.br
.RI "\fIDraw an ASCII art thin polyline\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_draw_circle\fP (\fBcaca_canvas_t\fP *, int, int, int, uint32_t)"
.br
.RI "\fIDraw a circle on the canvas using the given character\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_draw_ellipse\fP (\fBcaca_canvas_t\fP *, int, int, int, int, uint32_t)"
.br
.RI "\fIDraw an ellipse on the canvas using the given character\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_draw_thin_ellipse\fP (\fBcaca_canvas_t\fP *, int, int, int, int)"
.br
.RI "\fIDraw a thin ellipse on the canvas\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_fill_ellipse\fP (\fBcaca_canvas_t\fP *, int, int, int, int, uint32_t)"
.br
.RI "\fIFill an ellipse on the canvas using the given character\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_draw_box\fP (\fBcaca_canvas_t\fP *, int, int, int, int, uint32_t)"
.br
.RI "\fIDraw a box on the canvas using the given character\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_draw_thin_box\fP (\fBcaca_canvas_t\fP *, int, int, int, int)"
.br
.RI "\fIDraw a thin box on the canvas\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_draw_cp437_box\fP (\fBcaca_canvas_t\fP *, int, int, int, int)"
.br
.RI "\fIDraw a box on the canvas using CP437 characters\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_fill_box\fP (\fBcaca_canvas_t\fP *, int, int, int, int, uint32_t)"
.br
.RI "\fIFill a box on the canvas using the given character\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_draw_triangle\fP (\fBcaca_canvas_t\fP *, int, int, int, int, int, int, uint32_t)"
.br
.RI "\fIDraw a triangle on the canvas using the given character\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_draw_thin_triangle\fP (\fBcaca_canvas_t\fP *, int, int, int, int, int, int)"
.br
.RI "\fIDraw a thin triangle on the canvas\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_fill_triangle\fP (\fBcaca_canvas_t\fP *, int, int, int, int, int, int, uint32_t)"
.br
.RI "\fIFill a triangle on the canvas using the given character\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_fill_triangle_textured\fP (\fBcaca_canvas_t\fP *cv, int coords[6], \fBcaca_canvas_t\fP *tex, float uv[6])"
.br
.RI "\fIFill a triangle on the canvas using an arbitrary-sized texture\&. \fP"
.in -1c
.SH "Detailed Description"
.PP 
These functions provide routines for primitive drawing, such as lines, boxes, triangles and ellipses\&. 
.SH "Function Documentation"
.PP 
.SS "__extern int caca_draw_line (\fBcaca_canvas_t\fP * cv, int x1, int y1, int x2, int y2, uint32_t ch)"
This function never fails\&.
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP The handle to the libcaca canvas\&. 
.br
\fIx1\fP X coordinate of the first point\&. 
.br
\fIy1\fP Y coordinate of the first point\&. 
.br
\fIx2\fP X coordinate of the second point\&. 
.br
\fIy2\fP Y coordinate of the second point\&. 
.br
\fIch\fP UTF-32 character to be used to draw the line\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
This function always returns 0\&. 
.RE
.PP

.PP
Referenced by caca_draw_box(), caca_draw_triangle(), and caca_fill_ellipse()\&.
.SS "__extern int caca_draw_polyline (\fBcaca_canvas_t\fP * cv, int const x[], int const y[], int n, uint32_t ch)"
Draw a polyline on the canvas using the given character and coordinate arrays\&. The first and last points are not connected, hence in order to draw a polygon you need to specify the starting point at the end of the list as well\&.
.PP
This function never fails\&.
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP The handle to the libcaca canvas\&. 
.br
\fIx\fP Array of X coordinates\&. Must have \fCn\fP + 1 elements\&. 
.br
\fIy\fP Array of Y coordinates\&. Must have \fCn\fP + 1 elements\&. 
.br
\fIn\fP Number of lines to draw\&. 
.br
\fIch\fP UTF-32 character to be used to draw the lines\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
This function always returns 0\&. 
.RE
.PP

.SS "__extern int caca_draw_thin_line (\fBcaca_canvas_t\fP * cv, int x1, int y1, int x2, int y2)"
This function never fails\&.
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP The handle to the libcaca canvas\&. 
.br
\fIx1\fP X coordinate of the first point\&. 
.br
\fIy1\fP Y coordinate of the first point\&. 
.br
\fIx2\fP X coordinate of the second point\&. 
.br
\fIy2\fP Y coordinate of the second point\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
This function always returns 0\&. 
.RE
.PP

.PP
Referenced by caca_draw_thin_triangle()\&.
.SS "__extern int caca_draw_thin_polyline (\fBcaca_canvas_t\fP * cv, int const x[], int const y[], int n)"
Draw a thin polyline on the canvas using the given coordinate arrays and with ASCII art\&. The first and last points are not connected, so in order to draw a polygon you need to specify the starting point at the end of the list as well\&.
.PP
This function never fails\&.
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP The handle to the libcaca canvas\&. 
.br
\fIx\fP Array of X coordinates\&. Must have \fCn\fP + 1 elements\&. 
.br
\fIy\fP Array of Y coordinates\&. Must have \fCn\fP + 1 elements\&. 
.br
\fIn\fP Number of lines to draw\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
This function always returns 0\&. 
.RE
.PP

.SS "__extern int caca_draw_circle (\fBcaca_canvas_t\fP * cv, int x, int y, int r, uint32_t ch)"
This function never fails\&.
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP The handle to the libcaca canvas\&. 
.br
\fIx\fP Center X coordinate\&. 
.br
\fIy\fP Center Y coordinate\&. 
.br
\fIr\fP Circle radius\&. 
.br
\fIch\fP UTF-32 character to be used to draw the circle outline\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
This function always returns 0\&. 
.RE
.PP

.SS "__extern int caca_draw_ellipse (\fBcaca_canvas_t\fP * cv, int xo, int yo, int a, int b, uint32_t ch)"
This function never fails\&.
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP The handle to the libcaca canvas\&. 
.br
\fIxo\fP Center X coordinate\&. 
.br
\fIyo\fP Center Y coordinate\&. 
.br
\fIa\fP Ellipse X radius\&. 
.br
\fIb\fP Ellipse Y radius\&. 
.br
\fIch\fP UTF-32 character to be used to draw the ellipse outline\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
This function always returns 0\&. 
.RE
.PP

.SS "__extern int caca_draw_thin_ellipse (\fBcaca_canvas_t\fP * cv, int xo, int yo, int a, int b)"
This function never fails\&.
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP The handle to the libcaca canvas\&. 
.br
\fIxo\fP Center X coordinate\&. 
.br
\fIyo\fP Center Y coordinate\&. 
.br
\fIa\fP Ellipse X radius\&. 
.br
\fIb\fP Ellipse Y radius\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
This function always returns 0\&. 
.RE
.PP

.SS "__extern int caca_fill_ellipse (\fBcaca_canvas_t\fP * cv, int xo, int yo, int a, int b, uint32_t ch)"
This function never fails\&.
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP The handle to the libcaca canvas\&. 
.br
\fIxo\fP Center X coordinate\&. 
.br
\fIyo\fP Center Y coordinate\&. 
.br
\fIa\fP Ellipse X radius\&. 
.br
\fIb\fP Ellipse Y radius\&. 
.br
\fIch\fP UTF-32 character to be used to fill the ellipse\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
This function always returns 0\&. 
.RE
.PP

.PP
References caca_draw_line()\&.
.SS "__extern int caca_draw_box (\fBcaca_canvas_t\fP * cv, int x, int y, int w, int h, uint32_t ch)"
This function never fails\&.
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP The handle to the libcaca canvas\&. 
.br
\fIx\fP X coordinate of the upper-left corner of the box\&. 
.br
\fIy\fP Y coordinate of the upper-left corner of the box\&. 
.br
\fIw\fP Width of the box\&. 
.br
\fIh\fP Height of the box\&. 
.br
\fIch\fP UTF-32 character to be used to draw the box\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
This function always returns 0\&. 
.RE
.PP

.PP
References caca_draw_line()\&.
.SS "__extern int caca_draw_thin_box (\fBcaca_canvas_t\fP * cv, int x, int y, int w, int h)"
This function never fails\&.
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP The handle to the libcaca canvas\&. 
.br
\fIx\fP X coordinate of the upper-left corner of the box\&. 
.br
\fIy\fP Y coordinate of the upper-left corner of the box\&. 
.br
\fIw\fP Width of the box\&. 
.br
\fIh\fP Height of the box\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
This function always returns 0\&. 
.RE
.PP

.SS "__extern int caca_draw_cp437_box (\fBcaca_canvas_t\fP * cv, int x, int y, int w, int h)"
This function never fails\&.
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP The handle to the libcaca canvas\&. 
.br
\fIx\fP X coordinate of the upper-left corner of the box\&. 
.br
\fIy\fP Y coordinate of the upper-left corner of the box\&. 
.br
\fIw\fP Width of the box\&. 
.br
\fIh\fP Height of the box\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
This function always returns 0\&. 
.RE
.PP

.SS "__extern int caca_fill_box (\fBcaca_canvas_t\fP * cv, int x, int y, int w, int h, uint32_t ch)"
This function never fails\&.
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP The handle to the libcaca canvas\&. 
.br
\fIx\fP X coordinate of the upper-left corner of the box\&. 
.br
\fIy\fP Y coordinate of the upper-left corner of the box\&. 
.br
\fIw\fP Width of the box\&. 
.br
\fIh\fP Height of the box\&. 
.br
\fIch\fP UTF-32 character to be used to draw the box\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
This function always returns 0\&. 
.RE
.PP

.PP
References caca_add_dirty_rect(), and caca_put_char()\&.
.PP
Referenced by caca_conio_clreol()\&.
.SS "__extern int caca_draw_triangle (\fBcaca_canvas_t\fP * cv, int x1, int y1, int x2, int y2, int x3, int y3, uint32_t ch)"
This function never fails\&.
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP The handle to the libcaca canvas\&. 
.br
\fIx1\fP X coordinate of the first point\&. 
.br
\fIy1\fP Y coordinate of the first point\&. 
.br
\fIx2\fP X coordinate of the second point\&. 
.br
\fIy2\fP Y coordinate of the second point\&. 
.br
\fIx3\fP X coordinate of the third point\&. 
.br
\fIy3\fP Y coordinate of the third point\&. 
.br
\fIch\fP UTF-32 character to be used to draw the triangle outline\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
This function always returns 0\&. 
.RE
.PP

.PP
References caca_draw_line()\&.
.SS "__extern int caca_draw_thin_triangle (\fBcaca_canvas_t\fP * cv, int x1, int y1, int x2, int y2, int x3, int y3)"
This function never fails\&.
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP The handle to the libcaca canvas\&. 
.br
\fIx1\fP X coordinate of the first point\&. 
.br
\fIy1\fP Y coordinate of the first point\&. 
.br
\fIx2\fP X coordinate of the second point\&. 
.br
\fIy2\fP Y coordinate of the second point\&. 
.br
\fIx3\fP X coordinate of the third point\&. 
.br
\fIy3\fP Y coordinate of the third point\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
This function always returns 0\&. 
.RE
.PP

.PP
References caca_draw_thin_line()\&.
.SS "__extern int caca_fill_triangle (\fBcaca_canvas_t\fP * cv, int x1, int y1, int x2, int y2, int x3, int y3, uint32_t ch)"
This function never fails\&.
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP The handle to the libcaca canvas\&. 
.br
\fIx1\fP X coordinate of the first point\&. 
.br
\fIy1\fP Y coordinate of the first point\&. 
.br
\fIx2\fP X coordinate of the second point\&. 
.br
\fIy2\fP Y coordinate of the second point\&. 
.br
\fIx3\fP X coordinate of the third point\&. 
.br
\fIy3\fP Y coordinate of the third point\&. 
.br
\fIch\fP UTF-32 character to be used to fill the triangle\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
This function always returns 0\&. 
.RE
.PP

.PP
References caca_put_char()\&.
.SS "__extern int caca_fill_triangle_textured (\fBcaca_canvas_t\fP * cv, int coords[6], \fBcaca_canvas_t\fP * tex, float uv[6])"
This function fails if one or both the canvas are missing
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP The handle to the libcaca canvas\&. 
.br
\fIcoords\fP The coordinates of the triangle (3{x,y}) 
.br
\fItex\fP The handle of the canvas texture\&. 
.br
\fIuv\fP The coordinates of the texture (3{u,v}) 
.RE
.PP
\fBReturns:\fP
.RS 4
This function return 0 if ok, -1 if canvas or texture are missing\&. 
.RE
.PP

.SH "Author"
.PP 
Generated automatically by Doxygen for libcaca from the source code\&.
