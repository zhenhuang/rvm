.TH "caca_transform" 3caca "Sun Feb 7 2016" "Version 0.99.beta19" "libcaca" \" -*- nroff -*-
.ad l
.nh
.SH NAME
caca_transform \- libcaca canvas transformation
.SS "Functions"

.in +1c
.ti -1c
.RI "__extern int \fBcaca_invert\fP (\fBcaca_canvas_t\fP *)"
.br
.RI "\fIInvert a canvas' colours\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_flip\fP (\fBcaca_canvas_t\fP *)"
.br
.RI "\fIFlip a canvas horizontally\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_flop\fP (\fBcaca_canvas_t\fP *)"
.br
.RI "\fIFlip a canvas vertically\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_rotate_180\fP (\fBcaca_canvas_t\fP *)"
.br
.RI "\fIRotate a canvas\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_rotate_left\fP (\fBcaca_canvas_t\fP *)"
.br
.RI "\fIRotate a canvas, 90 degrees counterclockwise\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_rotate_right\fP (\fBcaca_canvas_t\fP *)"
.br
.RI "\fIRotate a canvas, 90 degrees counterclockwise\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_stretch_left\fP (\fBcaca_canvas_t\fP *)"
.br
.RI "\fIRotate and stretch a canvas, 90 degrees counterclockwise\&. \fP"
.ti -1c
.RI "__extern int \fBcaca_stretch_right\fP (\fBcaca_canvas_t\fP *)"
.br
.RI "\fIRotate and stretch a canvas, 90 degrees clockwise\&. \fP"
.in -1c
.SH "Detailed Description"
.PP 
These functions perform horizontal and vertical canvas flipping\&. 
.SH "Function Documentation"
.PP 
.SS "__extern int caca_invert (\fBcaca_canvas_t\fP * cv)"
Invert a canvas' colours (black becomes white, red becomes cyan, etc\&.) without changing the characters in it\&.
.PP
This function never fails\&.
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP The canvas to invert\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
This function always returns 0\&. 
.RE
.PP

.PP
References caca_add_dirty_rect()\&.
.SS "__extern int caca_flip (\fBcaca_canvas_t\fP * cv)"
Flip a canvas horizontally, choosing characters that look like the mirrored version wherever possible\&. Some characters will stay unchanged by the process, but the operation is guaranteed to be involutive: performing it again gives back the original canvas\&.
.PP
This function never fails\&.
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP The canvas to flip\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
This function always returns 0\&. 
.RE
.PP

.PP
References caca_add_dirty_rect(), and CACA_MAGIC_FULLWIDTH\&.
.SS "__extern int caca_flop (\fBcaca_canvas_t\fP * cv)"
Flip a canvas vertically, choosing characters that look like the mirrored version wherever possible\&. Some characters will stay unchanged by the process, but the operation is guaranteed to be involutive: performing it again gives back the original canvas\&.
.PP
This function never fails\&.
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP The canvas to flop\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
This function always returns 0\&. 
.RE
.PP

.PP
References caca_add_dirty_rect()\&.
.SS "__extern int caca_rotate_180 (\fBcaca_canvas_t\fP * cv)"
Apply a 180-degree transformation to a canvas, choosing characters that look like the upside-down version wherever possible\&. Some characters will stay unchanged by the process, but the operation is guaranteed to be involutive: performing it again gives back the original canvas\&.
.PP
This function never fails\&.
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP The canvas to rotate\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
This function always returns 0\&. 
.RE
.PP

.PP
References caca_add_dirty_rect(), and CACA_MAGIC_FULLWIDTH\&.
.SS "__extern int caca_rotate_left (\fBcaca_canvas_t\fP * cv)"
Apply a 90-degree transformation to a canvas, choosing characters that look like the rotated version wherever possible\&. Characters cells are rotated two-by-two\&. Some characters will stay unchanged by the process, some others will be replaced by close equivalents\&. Fullwidth characters at odd horizontal coordinates will be lost\&. The operation is not guaranteed to be reversible at all\&.
.PP
Note that the width of the canvas is divided by two and becomes the new height\&. Height is multiplied by two and becomes the new width\&. If the original width is an odd number, the division is rounded up\&.
.PP
If an error occurs, -1 is returned and \fBerrno\fP is set accordingly:
.IP "\(bu" 2
\fCEBUSY\fP The canvas is in use by a display driver and cannot be rotated\&.
.IP "\(bu" 2
\fCENOMEM\fP Not enough memory to allocate the new canvas size\&. If this happens, the previous canvas handle is still valid\&.
.PP
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP The canvas to rotate left\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
0 in case of success, -1 if an error occurred\&. 
.RE
.PP

.PP
References caca_add_dirty_rect()\&.
.SS "__extern int caca_rotate_right (\fBcaca_canvas_t\fP * cv)"
Apply a 90-degree transformation to a canvas, choosing characters that look like the rotated version wherever possible\&. Characters cells are rotated two-by-two\&. Some characters will stay unchanged by the process, some others will be replaced by close equivalents\&. Fullwidth characters at odd horizontal coordinates will be lost\&. The operation is not guaranteed to be reversible at all\&.
.PP
Note that the width of the canvas is divided by two and becomes the new height\&. Height is multiplied by two and becomes the new width\&. If the original width is an odd number, the division is rounded up\&.
.PP
If an error occurs, -1 is returned and \fBerrno\fP is set accordingly:
.IP "\(bu" 2
\fCEBUSY\fP The canvas is in use by a display driver and cannot be rotated\&.
.IP "\(bu" 2
\fCENOMEM\fP Not enough memory to allocate the new canvas size\&. If this happens, the previous canvas handle is still valid\&.
.PP
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP The canvas to rotate right\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
0 in case of success, -1 if an error occurred\&. 
.RE
.PP

.PP
References caca_add_dirty_rect()\&.
.SS "__extern int caca_stretch_left (\fBcaca_canvas_t\fP * cv)"
Apply a 90-degree transformation to a canvas, choosing characters that look like the rotated version wherever possible\&. Some characters will stay unchanged by the process, some others will be replaced by close equivalents\&. Fullwidth characters will be lost\&. The operation is not guaranteed to be reversible at all\&.
.PP
Note that the width and height of the canvas are swapped, causing its aspect ratio to look stretched\&.
.PP
If an error occurs, -1 is returned and \fBerrno\fP is set accordingly:
.IP "\(bu" 2
\fCEBUSY\fP The canvas is in use by a display driver and cannot be rotated\&.
.IP "\(bu" 2
\fCENOMEM\fP Not enough memory to allocate the new canvas size\&. If this happens, the previous canvas handle is still valid\&.
.PP
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP The canvas to rotate left\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
0 in case of success, -1 if an error occurred\&. 
.RE
.PP

.PP
References caca_add_dirty_rect()\&.
.SS "__extern int caca_stretch_right (\fBcaca_canvas_t\fP * cv)"
Apply a 270-degree transformation to a canvas, choosing characters that look like the rotated version wherever possible\&. Some characters will stay unchanged by the process, some others will be replaced by close equivalents\&. Fullwidth characters will be lost\&. The operation is not guaranteed to be reversible at all\&.
.PP
Note that the width and height of the canvas are swapped, causing its aspect ratio to look stretched\&.
.PP
If an error occurs, -1 is returned and \fBerrno\fP is set accordingly:
.IP "\(bu" 2
\fCEBUSY\fP The canvas is in use by a display driver and cannot be rotated\&.
.IP "\(bu" 2
\fCENOMEM\fP Not enough memory to allocate the new canvas size\&. If this happens, the previous canvas handle is still valid\&.
.PP
.PP
\fBParameters:\fP
.RS 4
\fIcv\fP The canvas to rotate right\&. 
.RE
.PP
\fBReturns:\fP
.RS 4
0 in case of success, -1 if an error occurred\&. 
.RE
.PP

.PP
References caca_add_dirty_rect()\&.
.SH "Author"
.PP 
Generated automatically by Doxygen for libcaca from the source code\&.
