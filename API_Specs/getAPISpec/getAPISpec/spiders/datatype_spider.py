# -*- coding: utf-8 -*-
import scrapy, sys
reload(sys)
sys.setdefaultencoding('utf8')

class DataTypeSpider(scrapy.Spider):
    name = "datatype"

    def __init__(self):
	self.API_type = ''

    def start_requests(self):
        urls = [
            'https://docs.microsoft.com/en-us/windows/desktop/winprog/windows-data-types',
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
	#print >>sys.stderr, '#', response.url, response.xpath('//title')
	#title = response.xpath('//title/text()').extract()
	for t in title:
		if t.find('function') >= 0:
			print '# URL', response.url
			func = t[:t.find('function')]
			print '# Function', func
			for c in response.xpath('//code'):
				l = c.css('::text').extract()
				print l[0]
				# return value description
				desc = ''
				for x in response.xpath("//h2[@id='return-value']/following-sibling::p").css('::text').extract():
					if desc == '':
						desc = x
					else:
						desc += x
				print '#', desc 
				# return value samples
				tab = response.xpath("//h2[@id='return-value']/following-sibling::table")
				header = ''
				for h in tab[0].xpath('//th/text()'):
					if header == '':
						header = h.extract()
					else:
						header += ',' + h.extract()
				print '# Header', header
				for tr in response.xpath("//h2[@id='return-value']/following-sibling::table/tr"):
					cell1 = ''
					for i in tr.xpath('td[1]/text()'):
						if cell1 == '':
							cell1 = i.extract()
						else:
							cell1 += i.extract() 
					cell2 = ''
					for i in tr.xpath('td[2]/text()'):
						if cell2 == '':
							cell2 = i.extract()
						else:
							cell2 += i.extract() 
					if cell1 or cell2:
						print '#', func, '==', cell1 + ' ; ' + cell2.strip()
				# we only need the function prototype
				break
				#print '\t', l[0].replace('\n', '')

