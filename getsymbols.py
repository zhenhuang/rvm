#!/usr/bin/python

# generate symbols from .lst file

import sys

inp = open(sys.argv[1], 'r')
state = 0
lineNo = 0
while True:
	line = inp.readline()
	if not line:
		break
	lineNo += 1
	line = line.strip()
	if not line:
		continue
	if state == 0:	
		if line.find('S U B R O U T I N E') > 0:
			state = 1
			func_line = lineNo
	elif state == 1:
		if line.find(';') > 0 and line.find('Attributes') == -1:
			#print lineNo, line
			addr = line.split(';')[0].strip()
			proto = line.split(';')[1].split()
			for p in proto:
				bracket = p.find('(')
				if bracket > 0:
					s = addr.split(':')[1] + ' ' + p[:bracket]
					if len(s.split()) != 2:
						print >>sys.stderr, lineNo, line
					else:
						print s
					break
			state = 0
	else:
		print 'Error'
inp.close()
