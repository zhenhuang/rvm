#!/usr/bin/python3.6
# Definition analysis

import angr, pyvex, sys, os, copy, claripy, networkx
from io import StringIO

class DefAnalysis():
	# public methods
	def get_def_for_block(self, addr):
		print('get_def_for_block', hex(addr), file=sys.stderr)
		if addr in self.data:
			self.defs = self.data[addr][0]
			self.addrs = self.data[addr][1]
			return

		self.data[addr] = [{}, []]
		self.defs = self.data[addr][0]
		self.addrs = self.data[addr][1]

		node = self.cfg.get_any_node(addr)
		if node is None or node.block is None:
			print('get_def_for_block cannot find node for', hex(addr), file=sys.stderr)
			return
		self.bits = node.block.vex.arch.bits
		for stmt in node.block.vex.statements:
			# parse IR in text seems to be easier than in object
			self.parse_IR(self.get_std_output(stmt.pp))
		self.parse_IR('PUT(offset=%d) = %s' % (node.block.vex.offsIP, self.get_std_output(node.block.vex.next.pp)))
		
	def init(self, cfg, func):
		self.cfg = cfg
		self.func = func
		self.data = {}
	
	# returns the operator and operands of a IF stmt if such stmt exists in the block
	def check_if(self):
		if 'IF' in self.defs:
			parts = self.defs['IF'].split('@')
			if len(parts) < 3:
				print('check_if', parts, file=sys.stderr)
				return None
			return (parts[0], parts[1], parts[2][:-1])
		else:
			return None

	def get_next_addr(self):
		if 'EIP' in self.defs:
			return self.defs['EIP']
		elif 'RIP' in self.defs:
			return self.defs['RIP']
		else:
			print('Error: no next code address!', file=sys.stderr)
			return None

	# private methods
	def get_std_output(self, f):
		out = StringIO()
		old_stdout = sys.stdout
		sys.stdout = out
		f()
		sys.stdout = old_stdout
		return out.getvalue()

	def __init__(self):
		self.offset_regs = {}
		self.init_offset_mappings()
		self.init_BinOp_Table()
		self.data = {}

	# get a code block
	def get_block(self, node_addr):
		for b in self.func.graph.nodes:
			if b.addr == node_addr:
				return b
		print('get_block returns None for', hex(self.func.addr), hex(node_addr), file=sys.stderr)
		return None

	def get_def(self, var):
		if self.is_constant(var):
			return var
		if var in self.defs:
			return self.defs[var]
		else:
			print('Warning', var, 'is not defined!', file=sys.stderr)
			return var

	def set_def(self, LHS, RHS):
		#print >>sys.stderr, LHS, '=', RHS
		self.defs[LHS] = RHS

	def load_register_mappings(self, bits, names, start, incr):
		self.offset_regs[bits] = {}
		t = start
		for r in names:
			self.offset_regs[bits][t] = r
			t += incr

	def init_offset_mappings(self):
		regs64bits = ['rax', 'rcx', 'rdx', 'rbx', 'rsp', 'rbp', 'rsi', 'rdi', 'r8', 'r9', 'r10', 'r11', 'r12', 'r13', 'r14', 'r15', 'CC_OP', 'CC_DEP1', 'CC_DEP2', 'CC_NDEP', 'DFLAG', 'RIP', 'ACFLAG', 'IDFLAG', 'FS_CONST']	
		regs32bits = ['eax', 'ecx', 'edx', 'ebx', 'esp', 'ebp', 'esi', 'edi', 'CC_OP', 'CC_DEP1', 'CC_DEP2', 'CC_NDEP', 'DFLAG', 'IDFLAG', 'ACFLAG', 'EIP']
		self.load_register_mappings(64, regs64bits, 16, 8)
		self.load_register_mappings(32, regs32bits, 8, 4)

	def offset_to_reg(self, off):
		try:
			ret = self.offset_regs[self.bits][off]
		except KeyError:
			print(self.bits, 'bits no supported at', self.addrs[-1], file=sys.stderr)
			ret = 'R' + str(off) + '_' + str(self.bits)
		return ret

	def is_constant(self, exp):
		return exp[0:2] == '0x'

	def conv_constant(self, v):
		# for 64 bit binary code
		val = int(v, 16)
		if val > 0x8000000000000000:
			return str(-(0x10000000000000000 - val))
		else:
			return str(val)

	def substitute_regs_parms(self, op):
		parms = op.split(',')
		for i in range(len(parms)):
			if not self.is_constant(parms[i]):
				parms[i] = self.get_def(parms[i])
			#else:
			#	parms[i] = self.conv_constant(parms[i])
		return parms

	def make_expr(self, parms, op):
		r = ''
		for p in parms:
			if not r:
				r = p
			else:
				r += op + p
		return r

	# Individual IR code parsers
	def IR_GET_I64(self, op):
		# assume op in the form 'offset=16'
		off = int(op[7:])
		return self.offset_to_reg(off)

	def IR_BinOp(self, op, oprand, cmp):
		parms = self.substitute_regs_parms(oprand)
		if cmp:
			return self.make_expr(parms, op) + '?'
		else:
			return self.make_expr(parms, op)

	def IR_64to32(self, op):
		return self.get_def(op)

	def IR_32Uto64(self, op):
		return self.get_def(op)
	
	def IR_1Uto64(self, op):
		return self.get_def(op)

	def IR_64to1(self, op):
		return self.get_def(op)

	def IR_LDle_I32(self, op):
		addr = '[' + self.get_def(op) + ']'
		return self.get_def(addr)

	def IR_LDle_I64(self, op):
		addr = '[' + self.get_def(op) + ']'
		return self.get_def(addr)

	def IR_STle(self, op):
		return '[' + self.get_def(op) + ']'

	def IR_PUT(self, op):
		# assume op in the form 'offset=16'
		off = int(op[7:])
		return self.offset_to_reg(off)

	# enclose comparison operator with a pair of '@' so that the operands can be recovered easily
	def init_BinOp_Table(self):
		# binop with nameN where N in (8, 16, 32, 64)
		# the second number in each tuple determines whether the op is a comparison
		comBinOp = [('Add', False, '+'), ('Sub', False, '-'), ('Mul', False, '*'), ('Or', False, '|'), ('And', False, '&'), ('Xor', False, '^'), ('Shl', False, '<<'), ('Shr', False, '>>'), ('Sar', False, '>>'), ('CmpEQ', True, '@==@'),  ('CmpNE', True, '@!=@')]
		self.ComBinOpTable = {}
		for bits in [8, 16, 32, 64]:
			for opcode, cmp, op in comBinOp:
				self.ComBinOpTable[opcode + str(bits)] = [op, cmp]
		#print self.ComBinOpTable

	def IR_CmpLT32S(self, op):
		return self.IR_BinOp('@< @', op, True)

	def IR_CmpLT64S(self, op):
		return self.IR_BinOp('@< @', op, True)

	def IR_CmpLE32S(self, op):
		return self.IR_BinOp('@<=@', op, True)

	def IR_CmpLE64S(self, op):
		return self.IR_BinOp('@<=@', op, True)

	def IR_CmpLT32U(self, op):
		return self.IR_BinOp('@< @', op, True)

	def IR_CmpLT64U(self, op):
		return self.IR_BinOp('@< @', op, True)

	def IR_CmpLE32U(self, op):
		return self.IR_BinOp('@<=@', op, True)

	def IR_CmpLE64U(self, op):
		return self.IR_BinOp('@<=@', op, True)

	def call_IR_func(self, call):
		bracket = call.find('(')
		OPCODE = call[:bracket]
		# method name cannot contain ':'
		OPCODE = OPCODE.replace(':', '_')
		OP = call[bracket+1:call.find(')')]
		if OPCODE in self.ComBinOpTable:
			op, cmp = self.ComBinOpTable[OPCODE]
			defn = self.IR_BinOp(op, OP, cmp)
		else:
			try:
				defn = getattr(self, 'IR_' + OPCODE)(OP)
			except AttributeError:
				print('Warning:', OPCODE, 'is not implemented yet!', file=sys.stderr)
				defn = ''
		return defn

	def extract_args(self, IR):
		p = IR.find('(')
		q = IR.find(')')
		return IR[p+1:q].split(',')
		
	def parse_IR(self, IR):
		err = False
		#print IR
		if IR.find('IMark') >= 0:
			self.addrs.append(self.extract_args(IR)[0])
			return
		elif IR[:3] == 'if ':
			args = self.extract_args(IR)
			# The block is supposed to have at most one if OP
			self.set_def('IF', self.get_def(args[0]))
			return
		elif IR.count('=') == 0:
			return
		elif IR.count('=') == 1:
			assn = IR.split('=')
		else:
			p = IR.find('(')
			q = IR.find(')')
			a = IR.find('=')
			b = IR.find('=', a + 1)
			if p < a:
				assn = [IR[:b], IR[b+1:]]
			else:
				assn = [IR[:a], IR[a+1:]]
		if len(assn) < 1:
			err = True
		else:
			# Supported IR forms: 
			# 1. t6 = t5
			# 2. t6 = FUNC(arg1[,arg2])
			# 	where FUNC can be GET:I64, Add64, LDle:I32, 32Uto64, 64to32, CmpEQ32, 1Uto64, 64to1, etc.
			# 3. STle(t5) = t7, PUT(rax) = t11
			err = False
			LHS = assn[0].strip()
			RHS = ''
			for a in assn[1:]:
				a=a.strip()
				if not RHS:
					RHS = a
				else:
					RHS+= '=' + a
			#print >>sys.stderr, 'LHS', LHS, 'RHS', RHS
			if LHS.find('(') > 0:
				defn = self.call_IR_func(LHS)
				if defn:
					self.set_def(defn, self.get_def(RHS))
			elif RHS.find('(') > 0:
				defn = self.call_IR_func(RHS)
				if defn:
					self.set_def(LHS, defn)
			elif LHS[0] == 't' and RHS[0] == 't':
				self.set_def(LHS, self.get_def(RHS))
			else:
				err = True
		if err:
			print('Warning: parse_IR does not support', IR, file=sys.stderr)
			return

def usage():
	print('Usage: defanalysis binary func_name|func_addr addr')
	sys.exit()

if __name__ == "__main__":
	if len(sys.argv) < 4:
		usage()
	binary = sys.argv[1]
	func_id = sys.argv[2]
	addr = sys.argv[3]
	proj = angr.Project(binary, auto_load_libs = False)
	cfg = proj.analyses.CFGFast()
	if func_id[:2] == '0x':
		func = cfg.kb.functions.function(addr=int(func_id, 0))
	else:
		func = cfg.kb.functions.function(name=func_id)
	if func is None:
		print('Function cannot be found!')
		sys.exit()
	analysis = DefAnalysis(cfg, func)
	analysis.get_def_for_block(int(addr, 0))
	for LHS in analysis.defs:
		print(LHS, ':', analysis.defs[LHS])
	print('Instructions:', analysis.addrs[0], '->', analysis.addrs[-1])


