#!/usr/bin/python

# generate error specifications from API specifications
import sys

def load_pointer_types(name):
	inp = open(name, 'r')
	while True:
		line = inp.readline()
		if not line:
			break
		line = line.strip()
		if not line:
			continue
		Pointer_Types.append(line)
	inp.close()

Pointer_Types = []
Unknown_Types = {}
ErrorForType = {}
# HRESULT: E_ABORT 0x80004004
known_types = {'int':'-1', 'HRESULT':'0x80004004', 'LRESULT':'0x80000001', 'UINT':'0x80000001', 'BOOL':'0', 'MMRESULT':'1', 'DWORD':'-1', 'LPMMIOPROC':'NULL', '_BOOL':'0', 'HWND':'NULL'}
if len(sys.argv) > 2:
	load_pointer_types(sys.argv[2])

inp = open(sys.argv[1], 'r')
lineNo = 0
count = 0
while True:
	line = inp.readline()
	if not line:
		break
	lineNo += 1
	line = line.strip()
	if not line:
		continue
	if line[0] == '#':
		continue
	if line.find('(') > 0:
		count += 1
		error = None
		ret_type = line.split()[0]
		name = None
		for f in line.split():
			if f.find('(') > 0:
				name = f[:f.find('(')]
				break
		if name is None:
			print >>sys.stderr, 'Error: unknown function name at line', lineNo, ':', line
			continue
		if ret_type in known_types:
			error = known_types[ret_type]
		else:
			if ret_type[:2] == 'LP' or ret_type[-4:] == 'PROC' or ret_type in Pointer_Types:
				error = 'NULL'
			else:
				for ret_type in line.split():
					if ret_type.find('('):
						break
					if ret_type in known_types:	
						error = known_types[ret_type]
		if error is None:
			print >>sys.stderr, 'Error: unknown return type at line', lineNo, ':', line
			if ret_type not in Unknown_Types:
				Unknown_Types[ret_type] = 0
			Unknown_Types[ret_type] += 1
		else:
			ErrorForType[name] = error
inp.close()
for name in ErrorForType:
	print name, ErrorForType[name]
for t in Unknown_Types:
	print >>sys.stderr, t, Unknown_Types[t]
print >>sys.stderr, 'Processed', lineNo, 'lines', count, 'functions', len(ErrorForType), 'identified'

