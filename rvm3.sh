#!/bin/bash
ANGR_DIR=~/.virtualenvs/angr3
RVM_DIR=~/Projects/RVM
. $ANGR_DIR/bin/activate
python3.6 $RVM_DIR/rvm3.py $*
