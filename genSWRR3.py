#!/usr/bin/python3.6
import sys, angr, defanalysis3, struct, time, archinfo

def usage():
	print('Usage: genSWRR binary_file functiona_name ERR_file symbol_file SWRR_file')
	print('Example: genSWRR.py webvw.dll CWebViewFolderIcon::setSlice webvw.dll.ERRs webvw.dll.syms SWRR_setSlice')
	sys.exit()

def load_SWRRs(name, func):
	inp = open(name ,'r')
	while True:
		line = inp.readline()
		if not line:
			break
		line = line.strip()
		if not line:
			continue
		if line.split()[0] == func:
			return line
	inp.close()
	print('Error:', func, 'not found in SWRR file!', file=sys.stderr)
	return None
		
def load_Symbols(name):
	syms = {}
	inp = open(name ,'r')
	while True:
		line = inp.readline()
		if not line:
			break
		line = line.strip()
		if not line:
			continue
		syms[line.split()[1]] = line.split()[0]
	inp.close()
	return syms

def makeSWRR(func, entry, ret_value, cfg, SWRRFile):
	if entry[0:2] != '0x':
		entry_addr = int('0x'+entry, 0)
	else:
		entry_addr = int(entry, 16)
	cfg_func = cfg.kb.functions.function(addr=entry_addr)
	if cfg_func is None:
		print('Error:', func, 'cannot be located in binary!', file=sys.stderr)
		return
	else:
		ret = cfg.get_any_node(cfg_func.ret_sites[0].addr)
		analysis = defanalysis3.DefAnalysis()
		analysis.init(cfg, func)
		analysis.get_def_for_block(ret.addr)
		first_inst_addr = int(analysis.addrs[0], 0)
		last_inst_addr = int(analysis.addrs[-1], 0)
		ret_inst_size = ret.size - last_inst_addr - first_inst_addr
		#print hex(last_inst_addr), ret_inst_size
		if ret_value == 'NULL':
			ret_value = 0
		value = struct.pack('<I', int(ret_value))
		#value = value.decode('utf-8')
		SWRR_str = '0xb8' # MOV EAX, ####
		for i in value:
			#SWRR_str += ' '+ hex(ord(i))
			SWRR_str += ' '+ hex(i)
		k = 0
		for i in ret.byte_string:
			if ret.addr + k >= last_inst_addr:
				#SWRR_str += ' ' + hex(ord(i))
				SWRR_str += ' ' + hex(i)
			k += 1
		output = open(SWRRFile, 'w')
		# output the function name and bytes representing the instructions of the SWRR
		print(func, hex(entry_addr), SWRR_str, file=output)
		# output 32 bytes starting from the entry address of the function
		ins = 'signature:'
		state = proj.factory.blank_state()
		for i in range(32):
			bv = state.memory.load(entry_addr + i, 1, endness=archinfo.Endness.LE)
			ins += ' ' + hex(state.solver.eval(bv))
		print(ins, file=output)
		output.close()

start_time = time.time()
if len(sys.argv) < 6:
	usage()
Binary = sys.argv[1]
FuncName = sys.argv[2]
ERRFile = sys.argv[3]
SymbolFile = sys.argv[4]
SWRRFile = sys.argv[5]

Syms = load_Symbols(SymbolFile)
line = load_SWRRs(ERRFile, FuncName)
if line is None:
	sys.exit()
proj = angr.Project(Binary, auto_load_libs = False)
cfg = proj.analyses.CFGFast()
parts = line.split()
l = (len(parts) - 1)//2
for i in range(l):
	func = parts[i*2+1]
	ret_value = parts[i*2 + 2]
	#print >>sys.stderr, func
	#print >>sys.stderr, ret_value
	makeSWRR(func, Syms[func], ret_value, cfg, SWRRFile)
end_time = time.time()
print('genSWRR time:', end_time - start_time)
