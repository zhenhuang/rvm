# RVM

Rapid Vulnerability Mitigation with Security Workarounds

Rapid Vulnerability Mitigation (RVM) is a tool that automatically generates Security Workarounds for Rapid Response (SWRRs) and instruments SWRRs into a binary to mitigate vulnerabilities rapidly. 

The work on RVM is published in the 2nd NDSS Workshop on Binary Analysis Research (BAR) 2019 (https://www.ndss-symposium.org/ndss-paper/auto-draft-37/).