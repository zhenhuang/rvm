#!/usr/bin/python3.6
import sys, subprocess

def usage():
	print("Usage: {} binary".format(sys.argv[0]))
	sys.exit()

if len(sys.argv) < 2:
	usage()
binary = sys.argv[1]
l=subprocess.check_output(["objdump", "-t", binary])
s=l.decode('utf-8')
count=0
for line in s.split('\n'):
	count+=1
	parts=line.strip().split()
	if len(parts)==6:
		if parts[2].strip() == 'F' and parts[3].strip() == '.text':
			print(hex(int(parts[0], 16)), parts[5].strip())
