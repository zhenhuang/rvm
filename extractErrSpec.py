#!/usr/bin/python

# generate error specifications from decompressed man pages
import sys

def remove_metadata(line):
	line = line.strip()
	# remove \fI and \fP, \fB
	for p in ['\\fI', '\\fP', '\\fB']:
		line = line.replace(p, '')
	# replace \- with -
	line = line.replace('\\-', '-')
	# remove .SH , .BR , .I , etc.
	out = ''
	k = 0
	while k < len(line):
		if line[k] == '.' and (k < len(line) - 1 and line[k + 1] != ' '):
			while k < len(line) and line[k] != ' ':
				k += 1
			k += 1
		if k < len(line):
			out += line[k]
			k += 1
	return out

def link_sentence(lines, start, end, prefix):
	i = start
	found = False
	while i <= end:
		line = remove_metadata(lines[i])
		if line[:len(prefix)] == prefix:
			s = line
			found = True
			print >>sys.stderr, '# line start', i + 1, line
		elif found:
			s += ' ' + line
		if line.find('.') >= 0:			
			break
		i += 1
	print  >>sys.stderr, '# line end', i + 1, line
	return i, s

def const2int(r):
	convert_list = {'zero':0, 'NULL':'NULL'}
	if r in convert_list:
		return convert_list[r]
	else:
		return None

def extract_error_ret_value(s):
	ret = None
	# 1 - prefix, 2 - suffix
	patterns = [('is returned', 1), ('returns', 2), ('return', 2), ('result value of', 2),  ('return value is', 2)]
	for p, dir in patterns:
		k = s.find(p)
		if k >= 0:
			if dir == 1:
				r = s[:k].split()[-1]
			elif dir == 2:
				r = s[k + len(p):].split()[0]
			r = r.strip(" ,.;")
				
			try:
				ret = int(r)
			except ValueError:
				ret = const2int(r)
			break
	return ret

def find_error_ret_value(lines, start, end):
	prefix_list = ['On error', 'Otherwise', 'On failure']
	infix_list = ['on error', 'on failure', 'for failure', 'if an error occur']
	ret = None
	i = start
	found = False
	while i <= end:
		line = remove_metadata(lines[i])
		for p in prefix_list:
			if len(line) > len(p):
				if line[:len(p)] == p:
					found = True
					i, s = link_sentence(lines, i, end, p)
					print >>sys.stderr, i + 1, s
					ret = extract_error_ret_value(s)
					break
		if found:
			break
		for p in infix_list:			
			k = line.find(p)
			if k >= 0:
				found = True
				if k == 0:
					line = remove_metadata(lines[i - 1]) + ' ' + line
					k = line.find(p)
				print >>sys.stderr, i + 1, line
				ret = line[:k].split() 
				if len(ret) > 0:
					ret = ret[-1]
					try:
						ret = int(ret)
					except ValueError:
						ret = const2int(ret)
				else:
					ret = extract_error_ret_value(line)
				break
		i += 1
	if found:
		print >>sys.stderr, '\t', ret	
	return i + 1, ret

def search_section(lines, start, end, title, section_headers, names):
	start, end = get_section(lines, start, end, title, section_headers)
	i = start
	ret = None
	while i <= end:
		i, ret = find_error_ret_value(lines, i, end)
		if ret is not None:
			for name in names:
				if isinstance(ret, int):
					if Name_Type[name][0][-1] != '*' and Name_Type[name][0] != 'void':
						Name_Type[name][1] = ret
				elif ret == 'NULL':
					if Name_Type[name][0][-1] == '*':
						Name_Type[name][1] = ret
				else:
					print >>sys.stderr, 'Warning: error return value does not match function return type'
	return i, ret

def search_error_value_section(lines, start, end, names):
	return search_section(lines, start, end, 'RETURN VALUE', ['.SH RETURN VALUE', '.SH "RETURN VALUE"', '.SH "RETURN VALUES"'], names)

def search_notes_section(lines, start, end, names):
	return search_section(lines, start, end, 'NOTES', ['.SH NOTES'], names)

def get_section(lines, start, end, sect_name, sect_headers):
	print >>sys.stderr, '# get_section', sect_headers, start + 1, end + 1

	found = False
	i = start + 1
	while i < end:
		line = lines[i].strip()
		#if found:
		#	print >>sys.stderr, line
		if found and line[:3] == '.SH':
			end = i - 1
			break
		if line in sect_headers:
			found = True
			start = i + 1
		i += 1
	if found:
		print >>sys.stderr, '#', sect_name, start + 1, end + 1
	return start, end

def get_name_type(lines, start, end):
	print >>sys.stderr, '# get_name_type', start + 1, end + 1
	names = []
	i = start + 1
	while i < end:
		line = lines[i].strip()
		if line == '.fi' or line[:3] == '.SH':
			break
		if len(line) > 4:
			format = 0
			if line[:4] == '.BI ':
				format = 1
			elif line[:3] == '\\fB':
				format = 2
			elif line[:3] == '.B ':
				format = 3
			q = -1
			if format == 1:
				q = line[4:].find('"')
				q += 5
			elif format == 2:
				q = 3
			elif format == 3:
				q = 3
			if q >= 0:
				#print >>sys.stderr, "Found .BI at line", i + 1
				b = line[q:].find('(')
				if b > 0:					
					parts = line[q:q + b].split()
					if len(parts) > 1:
						if parts[0] == 'typedef':
							i += 1
							continue
						ret = parts[0]
						for k in range(1, len(parts)-1):
							ret += ' ' + parts[k]
						name = parts[-1]
						name = name.replace('\\fR', '')
						name = name.replace('\\fB', '')
						if name[0] == '*':
							name = name[1:]
							ret += '*'
						name = name.strip()
						ret = ret.strip().replace('"', '')
						if name != '' and ret != '':
							print >>sys.stderr, name + ',' + ret, '#', i + 1
							if not ret in ['void', 'float', 'double', 'long double']:
								Name_Type[name] = [ret, None]
								if not ret in Type_Name:
									Type_Name[ret] = set()
								Type_Name[ret].add(name)
								names.append(name)
						else:
							print >>sys.stderr, 'no function name or type #', i + 1
				#else:
				#	print >>sys.stderr, "No bracket for .BI at line", i + 1
			#else:
			#	print >>sys.stderr, "Unexpected at line", i + 1
		i += 1
	return i, names

def process_one_page(lines, start, end):
	print >>sys.stderr, '# process_one_page', start + 1, end + 1
	start, names = get_name_type(lines, start, end)
	start, ret = search_error_value_section(lines, start, end, names)
	if ret is None:
		start, ret = search_notes_section(lines, start, end, names)

def contain_synopsis(line):
	for p in ['.SH SYNOPSIS', '.SH "SYNOPSIS"']:
		if len(line) >= len(p):
			if line[:len(p)] == p:
				return True
	return False

def usage():
	print "Usage: extractErrSpec Linux_API_docs.txt Linux_API_error.txt 2>err"
	sys.exit()

Name_Type = {} # e.g. setfsuid: [int, -1]
Type_Name = {}
if len(sys.argv) != 3:
	usage()
doc_file = sys.argv[1]
out_file = sys.argv[2]
inp = open(doc_file, 'r')
outp = open(out_file, 'w')
lineNo = 0
lines = inp.readlines()
inp.close()
while True:
	if lineNo >= len(lines):
		break
	line = lines[lineNo]
	line = line.strip()
	if not line:
		lineNo += 1
		continue
	if contain_synopsis(line):
		for i in range(lineNo + 1, len(lines)):
			if contain_synopsis(lines[i]):
				break
		process_one_page(lines, lineNo, i - 1)	
		lineNo = i - 1
	else:
		lineNo += 1

count_int_extracted = 0
count_ptr_extracted = 0
count_ptr = 0
print >>sys.stderr, 'Functions whose return value is not extracted'
for name in Name_Type:
	if Name_Type[name][0][-1] == '*':
		count_ptr += 1
	if Name_Type[name][1] is not None:
		if Name_Type[name][0][-1] == '*':
			count_ptr_extracted += 1
		else:
			count_int_extracted += 1
		print >>outp, name, Name_Type[name][1], Name_Type[name][0]
	else:
		print >>sys.stderr, '\t', name, Name_Type[name]
		if Name_Type[name][0][-1] == '*':
			print >>outp, name, 'NULL', Name_Type[name][0]
outp.close()
print >>sys.stderr, '# Functions return pointer:', count_ptr
print >>sys.stderr, '#\tError return value extracted:', count_ptr_extracted
print >>sys.stderr, '# Functions return int:', len(Type_Name['int'])
print >>sys.stderr, '#\tError return value extracted:', count_int_extracted
print >>sys.stderr, '# Processed', lineNo, 'lines', len(Name_Type), 'functions identified'

