#!/usr/bin/python
# RVM - Talos's frontend for binary code
import angr, pyvex, sys, os, copy, claripy, networkx, time, archinfo
from cStringIO import StringIO
import defanalysis

def usage():
	print "Usage: rvm binary_code [APISpecFile] [SymFile]"
	sys.exit()

def concat_with_sep(farg, *args):
	sep = farg
	string = ''
	for arg in args:
		if string:
			string += '@' + str(arg)
		else:
			string = arg
	return string

def output_info(f, func_name, addr, target, target_name, cdep, access, kind, key, follower):
	#print >>sys.stderr,func_name,'calls',target_name,'at',hex(addr)
	path, filename = os.path.split(inputFile)
	output = open(outputFile, 'a')
	#print >>output, concat_with_sep('@', filename, path, filename, hex(addr), func_name, hex(addr), cdep, target, key, -1, 0) 
	print >>output, concat_with_sep('@', filename, path, filename, hex(addr), func_name, hex(addr), cdep, target_name, key, access, kind + follower) 
	output.close()

def output_call(f, func_name, addr, target, target_name, cdep, follower):
	output_info(f, func_name, addr, target, target_name, cdep, '-1', '0', '', follower)

def output_ret(f, func_name, addr, cdep, ret_value):
	output_info(f, func_name, addr, '', '', cdep, '-2', '7', ret_value, '')

def output_return_call(f, func_name, addr, target, target_name, cdep, follower):
	output_info(f, func_name, addr, target, target_name, cdep, '-2', '8', '', follower)

def output_checked_call(f, func_name, addr, target, target_name, cdep, cond):
	output_info(f, func_name, addr, target, target_name, cdep, '-2', '12', cond, '')

def output_cdep(f, func_name, addr, cdep):
	output_info(f, func_name, addr, '', '', cdep, '-2', '3', '', '')

# return a list of of (addr, node, ret_value) tuples, each for one return block
# the returned list should be cached for each func to improve performance
#
# the same block might be scanned more than once, one for each return block
# return a list of tuples in the form of (ret_site_addr, basicblock, return_value, control_dep)
def find_ret_values(cfg, func):
	print >>sys.stderr, 'find_ret_values', get_func_name(func)
	if func in cached_ret_values:
		return cached_ret_values[func]
	l = []
	for ret in func.ret_sites:
		retb = cfg.get_any_node(ret.addr)
		rs = find_last_put_const_to_reg(func.addr, 16, retb)
		print >>sys.stderr,'returns', rs
		for r in rs:
			l.append([ret.addr, r[0], r[1], ''])
	cached_ret_values[func] = l
	return l

def set_ret_cdep(ret, cdep):
	ret[3] = cdep

# if there is (at least) an assignment of rax on a path that leads to 
# the ret block
def find_last_put_const_to_reg_ex(func_addr, i, node, visited):
	if node in visited:
		return []
	visited.add(node)
	print >>sys.stderr, 'find_last_put_const_to_reg_ex', hex(node.addr)
	if node.block is None:
		return []
	# check if the block contain such return
	#stmts = copy.deepcopy(node.block.vex.statements)
	#stmts.reverse()
	for stmt in reversed(node.block.vex.statements):
		if isinstance(stmt, pyvex.stmt.Put) and stmt.offset == i:
			if isinstance(stmt.data, pyvex.expr.Const):
				print >>sys.stderr, '\t', 'returns', stmt.data.con.value
				return [(node, stmt.data.con.value)]
			else:
				return []
	# if the block doesn't contain such return, look into predecessors of the block
	vals = []
	for pred in node.predecessors:
		if pred.function_address == func_addr:
			retv = find_last_put_const_to_reg_ex(func_addr, i, pred, visited)
			if retv:
				vals.extend(retv)
	# the return value from all predecessors must converge
	print >>sys.stderr,'\t','vals=',vals
	#lastv = None
	#for (n, v) in vals:
	#	if lastv is None:
	#		lastv = v
	#	elif lastv != v:
	#		return None
	return vals

# if there is (at least) an assignment of rax on a path that leads to 
# the ret block
def find_last_put_const_to_reg(func_addr, i, node):
	visited = set()
	return find_last_put_const_to_reg_ex(func_addr, i, node, visited)

# get a code block
def get_block(func, node_addr):
	for b in func.graph.nodes:
		if b.addr == node_addr:
			return b
	print >>sys.stderr, 'get_block returns None for', hex(func.addr), hex(node_addr)
	return None

def get_guardians_by_block(cfg, cdg, func, node_addr):
	block = get_block(func, node_addr)
	print >>sys.stderr,'block', block
	preds = block.predecessors()
	print >>sys.stderr,'pred', preds
	# remove interprocedural flow
	for p in preds:
		if not isinstance(p, angr.codenode.BlockNode):
			preds.remove(p)
	if len(preds) == 1:
		pnode = cfg.get_any_node(preds[0].addr)
		print >>sys.stderr,'get_guardians_by_block', hex(pnode.addr), 'returns', cdg.get_guardians(pnode)
		return cdg.get_guardians(pnode)
	else:
		print >>sys.stderr,'get_guardians_by_block', hex(node_addr), 'returns []'
		return []

# get control dependency of the instruction at addr
#	returns addr;branch[,addr;branch...]
def get_guardians(cfg, cdg, node, func_addr):
	# restrict guardians to be intraprocedural
	# fixup the guardians for the node right after a call
	func = cfg.kb.functions[func_addr]
	g = cdg.get_guardians(node)
	s = set()
	t = set() 
	#print >>sys.stderr,'g:',g
	for gnode in g:
		#print >>sys.stderr,'gnode', hex(gnode.function_address), hex(func_addr)
		if gnode.function_address != func_addr:
			s.add(gnode)
			for knode in get_guardians_by_block(cfg, cdg, func, node.addr):
				if knode.function_address == func_addr:
					t.add(knode)
	#print >>sys.stderr,'g:',g
	#print >>sys.stderr,'s:',s
	#print >>sys.stderr,'t:',t
	for snode in s:
		g.remove(snode)
	for tnode in t:
		g.append(tnode)
	print >>sys.stderr,'get_guardians returns', node, g
	return g

def add_cdep(addr, cond_block_addr, branch):
	#print >>sys.stderr, 'add_cdep', addr, cond_block_addr, branch
	print >>sys.stderr, 'add_cdep', hex(addr), hex(cond_block_addr), branch
	if not addr in control_dep:
		control_dep[addr] = set()
	control_dep[addr].add((cond_block_addr, branch))

# mark control dependency for one branch of a conditional statement
# 	start: start address
#	end: end address
#	branch: 0 - if branch, 1 - else branch
#	return the address of the branch's post-dominator
def mark_one_branch(func, cfg, cond_block, start, end, branch):
	queue = []
	visited = []
	queue.append(start)
	while (len(queue) > 0):
		b = queue.pop(0)
		visited.append(b)
		#if b >= end:
		#	continue
		add_cdep(b, cond_block.addr, branch)
		code_block = get_block(func, b)
		if code_block is not None:
			for s in code_block.successors():
				if s.addr in visited:
					continue
				if cfg.get_any_node(s.addr) is None:
					print >>sys.stderr, 'Warning: mark_one_branch', hex(s.addr), 'does not belong to any BB!'
					continue
				if s.addr < end and cfg.get_any_node(s.addr).function_address == cond_block.function_address:
					queue.append(s.addr)

# mark BBs in if branch of a stmt
# we leave the marking of BBs in else branch in get_cdep
# output:
#	mapping from block address to stmt,0 for if branch
def mark_guardian_followers(cfg, block, stmt):
	# avoid duplicate work - seem to happen frequently
	if block.addr in guardian_followers:
		return
	guardian_followers.add(block.addr)
	print >>sys.stderr, 'mark_guardian_followers', hex(block.addr)
	if_branch_start = stmt.dst.value
	else_branch_start = block.block.vex.default_exit_target
	# ensure if_branch_start is smaller than else_branch_start
	if if_branch_start > else_branch_start:
		t = if_branch_start
		if_branch_start = else_branch_start
		else_branch_start = t
	mark_one_branch(cfg.kb.functions.function(addr=block.function_address), cfg, block, if_branch_start, else_branch_start, 0)

def get_cdep(cfg, cdg, addr, func_addr):
	print >>sys.stderr, 'get_cdep', hex(addr)
	cdep = set()
	node = cfg.get_any_node(addr)
	for gnode in get_guardians(cfg, cdg, node, func_addr):
		g = gnode.block.vex.statements[-1]
		if isinstance(g, pyvex.stmt.Exit):
			mark_guardian_followers(cfg, gnode, g)
			#print >>sys.stderr, '\t', g
			#print >>sys.stderr, hex(g.dst.value)
			#if addr == g.dst.value:
			#	branch = 0
			#else:
			#	branch = 1
			#if cdep:
			#	cdep += ','
			#cdep += str(hex(gnode.addr))+';'+str(branch)
			if addr in control_dep:
				for c in control_dep[addr]:
					cdep.add(c)
			else:
				add_cdep(addr, gnode.addr, 1)
				cdep.add((gnode.addr, 1))
		else:
			print >>sys.stderr, 'Warning: block containing', addr, 'does not end with Exit'
	out = ''
	for c in cdep:
		if out:
			out += ','
		out += str(hex(c[0])) + ';' + str(c[1])
	return out


# convert hex numbers into decimal numbers
def conv_constant(val):
	try:
		r = str(int(val, 0))
	except ValueError:
		r = None
	return r

# figure out how return value is used
# 	perform copy propagation
#	assume node is a call cfg node
# 	stop at the first condition Exit encountered for each successor of the call
#	return a check on the return value if such check exists
def find_checked_call(cfg, func, call_node):
	print >>sys.stderr, 'find_checked_call', hex(call_node.addr)
	dep = set()
	code = get_block(func, call_node.addr)
	for b in code.successors():
		if isinstance(b, angr.codenode.BlockNode):
			analysis.get_def_for_block(b.addr)
			t = analysis.check_if()
			if t is not None and t[0] == 'rax':
				print >>sys.stderr, '\tadd', t[2]+','+t[1]
				v = conv_constant(t[2])
				if v is not None:
					dep.add((b.addr, v+','+t[1]))
	# if there are ever more than successors, each of which has a check on return value,
	# then they are supposed to have the same check
	if len(dep) == 1:
		return dep.pop()
	else:
		return (None, None)

# return a list of intraprocedural post dominators of node
# using cfg of the entire program might be inefficient
def get_postdominators(cfg, node):
	if node in cached_post_dominators:
		return cached_post_dominators[node]
	l = []
	print >>sys.stderr,'get_postdominators', node
	for d in networkx.immediate_dominators(cfg.graph, node):
		if d != node and d.function_address == node.function_address:
			 l.append(d)
	cached_post_dominators[node] = l
	return l

# traverse intraprocedurally from the start node to the end node (excluding start)
# stops if call_back function returns True
# returns False if call_back never returns True for every traversed block
def traverse_between(func, cfg, start, end, call_back):
	queue = []
	visited = []
	queue.append(start.addr)
	while (len(queue) > 0):
		b = queue.pop(0)
		visited.append(b)
		if b != start.addr and call_back(cfg, func, cfg.get_any_node(b)):
			return True
		code_block = get_block(func, b)
		if code_block is not None:
			for s in code_block.successors():
				if s.addr in visited:
					continue
				if cfg.get_any_node(s.addr) is None:
					continue
				if cfg.get_any_node(s.addr).function_address == func.addr:
					queue.append(s.addr)
	return False

# returns True if the specified register is modified in a node
def is_reg_modified(cfg, func, addr, reg):
	analysis.get_def_for_block(addr)
	return reg in analysis.defs

# returns True if the node contains a call or a modification to register RAX
def return_call_check(cfg, func, node):
	print >>sys.stderr, "return_call_check at", hex(node.addr)
	if node.addr in cached_return_call_check:
		return cached_return_call_check[node.addr]
	ret = node in func.get_call_sites()
	print >>sys.stderr, "\tcontains a call?", ret
	if not ret:
		ret = is_reg_modified(cfg, func, node.addr, 'rax')
		print >>sys.stderr, "\tmodifies RAX?", ret
	cached_return_call_check[node.addr] = ret
	return ret

def is_return_call(func, cfg, callnode, retnode):
	print >>sys.stderr, "is_return_call", hex(callnode.addr), hex(retnode.addr)
	return not traverse_between(func, cfg, callnode, retnode, return_call_check)

def find_const_returns(cfg, func, f):
	ret_values = find_ret_values(cfg, func)
	for r in ret_values:
		cdep = get_cdep(cfg, cdg, r[1].addr, func.addr)
		set_ret_cdep(r, cdep)
		output_ret(f, get_func_name(func), r[1].addr, cdep, r[2])
	return ret_values

def identify_func_return_types(cfg):
	Function_return_types = {}
	for func in cfg.kb.functions:
		Function_return_types[get_func_name(func)] = 'Unknown'
	for f in API_return_types:
		Function_return_types[f] = API_return_types[f]
	for func in cfg.kb.functions:
		for call in func.get_call_sites():
			target = func.get_call_target(call)
			target_func = cfg.kb.functions.function(addr=target)

# handles indirect calls			
def get_call_target(cfg, func, call):
	print >>sys.stderr, 'get_call_target', call
	target = func.get_call_target(call)
	if cfg.get_any_node(target).size != 0:
		return target
	else:
		analysis.get_def_for_block(call)
		target = analysis.get_next_addr()
		if target[0] == '[':
			# indirect call
			addr = target[1:-1]
			s=proj.factory.blank_state()
			try:
				addr = int(addr, 0)
			except ValueError:
				print >>sys.stderr, 'Warning: get_call_target for', hex(call), '->', target
				return None
			a=s.memory.load(addr, s.arch.bits/8, endness=archinfo.Endness.LE)
			target = s.solver.eval(a)
			print >>sys.stderr, 'get_call_target for', hex(call), '->', hex(target)
			return target
		else:
			print >>sys.stderr, 'Warning: get_call_target for', hex(call), '->', target
			return None
		
def build_pdom(cfg, func):
	# choose to use code blocks (BlockNode) instead of CFGNodes, because CFGNode considers callee as successor of callsite
	print >>sys.stderr, 'build_pdom', hex(func.addr)
	pdom = {}
	nodes = func.graph.nodes.keys()
	enodes = func.ret_sites
	for n in nodes:
		if n in enodes:
			pdom[n.addr] = set([n.addr])
		else:
			pdom[n.addr] = set([x.addr for x in nodes])
	change = True
	while change:
		change = False
		for n in nodes:
			tmp = set()
			first = True
			for x in n.successors():
				if not x.addr in pdom:
					continue
				if first:
					tmp = pdom[x.addr].copy()
					first = False
				else:
					tmp = tmp.intersection(pdom[x.addr])
			tmp.add(n.addr)
			if tmp != pdom[n.addr]:
				#print >>sys.stderr, '\t', hex(n.addr)
				#print >>sys.stderr, '\told:', [hex(x) for x in pdom[n.addr]]
				#print >>sys.stderr, '\tnew:', [hex(x) for x in tmp]
				pdom[n.addr] = tmp.copy()
				change = True
	return pdom

def run_for_cfg(cfg, visited):
	# identify_func_return_types(cfg)
	if len(SpecFuncs) > 0:
		special = True
	else:
		special = False
	for f in cfg.kb.functions.keys():
		if f in visited:
			continue		
		func = cfg.kb.functions[f]
		if special and (not get_func_name(func) in SpecFuncs):
			continue
		visited.append(f)
		print >>sys.stderr, 'Processing', hex(func.addr), get_func_name(func)
		#func.normalize()
		analysis.init(cfg, func)
		pdom = build_pdom(cfg, func)
		if func.size > 0:
			FunctionLines.append(get_func_name(func))			
		ret_values = find_const_returns(cfg, func, f)
		for call in func.get_call_sites():
			target = get_call_target(cfg, func, call) # func.get_call_target(call)
			#target = func.get_call_target(call)
			target_func = cfg.kb.functions.function(addr=target)
			if target_func is None:
				continue
			if special:
				SpecFuncs.add(get_func_name(target_func))
			cdep = get_cdep(cfg, cdg, call, func.addr)
			follower = ''
			callb = cfg.get_any_node(call)
			condaddr, cond = find_checked_call(cfg, func, callb)
			if cond is not None:
				output_checked_call(f, get_func_name(func), condaddr, target, get_func_name(target_func), cdep, cond)
			callPrinted = False
			print >>sys.stderr,'postdominators for', hex(call)
			#for dnode in get_postdominators(cfg, callb):
			for d in pdom[callb.addr]:
				dnode = cfg.get_any_node(d)
				print >>sys.stderr,'\t', dnode
				#print >>sys.stderr,'\tret_values', ret_values
				for ret in ret_values:
					#print >>sys.stderr, '\tret', ret
					#print >>sys.stderr, '\tcdep', cdep
					if dnode == ret[1] and ret[3] == cdep:
						follower = ','+str(hex(ret[0]))+','+str(ret[2])
						print >>sys.stderr,'follower', follower
						output_call(f, get_func_name(func), call, target, get_func_name(target_func), cdep, follower)
						callPrinted = True
						break
			for retb in func.ret_sites:
				if is_return_call(func, cfg, callb, retb):
					output_return_call(f, get_func_name(func), call, target, get_func_name(target_func), cdep, follower)
					callPrinted = True
					break
			if not callPrinted:
				output_call(f, get_func_name(func), call, target, get_func_name(target_func), cdep, '')
		for ret in ret_values:
			output_cdep(f, get_func_name(func), ret[1].addr, ret[3])

def load_API_return_types(specFile):
	try:
		input = open(specFile, 'r')
	except IOError:
		print >>sys.stderr, specFile, 'does not exist!'
		return
	while True:
		line = input.readline()
		if not line:
			break
		line = line.strip()
		if not line:
			continue
		if line[0] == '#':
			continue
		if line[-1] == '(':
			parts = line.split()
			if len(parts) >= 2:
				return_type = parts[0].strip()
				func_name = parts[1].strip()[:-1]
				API_return_types[func_name] = return_type
	input.close()
	#print API_return_types

def get_func_name(func):
	if func.name[:4] == 'sub_':
		if func.addr in glob_func_names:
			return glob_func_names[func.addr]
		else:
			glob_func_names[func.addr] = func.name
			#print 'Error:', hex(func.addr), 'has no name!'
			return func.name
	else:
		return func.name

# symbol file format:
# 	func_entry_addr func_name
def load_Symbols(file):
	input = open(file, 'r')
	while True:
		line = input.readline()
		if not line:
			break
		line = line.strip()
		if not line:
			continue
		if line[0] == '#':
			continue
		parts = line.split()
		glob_func_names[int(parts[0], 16)] = parts[1].strip()
	input.close()
	print >>sys.stderr, len(glob_func_names), 'Symbols:'
	#print >>sys.stderr, glob_func_names

def output_lines_file(file, functions):
	name = file + '.lines'
	output = open(name, 'w')
	for func in functions:
		print >>output, '*Analyzed 0 lines for', func, file, 'Unknown(Unknown)'
	output.close()

def print_time(out):
	print >>out, 'angr time', angr_time_end - angr_time_start
	print >>out, 'RVM time:', end_time - start_time - (angr_time_end - angr_time_start)
	print >>out, 'Total time:', end_time - start_time

# Global variables
glob_func_names = {}
cached_ret_values = {}
cached_post_dominators = {}
cached_return_call_check = {}
control_dep = {}
guardian_followers = set()
API_return_types = {}
FunctionLines = []

#
start_time = time.time()
if len(sys.argv) < 3:
	usage()
inputFile = sys.argv[1]
outputFile = sys.argv[2]
if len(sys.argv) > 3:
	APISpecFile = sys.argv[3]
else:
	APISpecFile = None
if len(sys.argv) > 4:
	SymFile = sys.argv[4]
else:
	SymFile = None
if APISpecFile:
	load_API_return_types(APISpecFile)
if SymFile:
	load_Symbols(SymFile)
if len(sys.argv) > 5:
	SpecFuncs = set([sys.argv[5]])
	print 'Specified only to process', SpecFuncs	
else:
	SpecFuncs = set()
angr_time_start = time.time()
proj = angr.Project(inputFile, load_options={'auto_load_libs':False})
tmp_kb = angr.KnowledgeBase(proj, proj.loader.main_object)
cfgfast = proj.analyses.CFGFast(kb=tmp_kb)
cfg = proj.analyses.CFGAccurate()
cdg = proj.analyses.CDG(cfg)
angr_time_end = time.time()

analysis = defanalysis.DefAnalysis()
# entry_func is often a _start function that calls __libc_start_main and then main
entry_func = proj.entry
visited_funcs = []
CountreachedFuncs = len(SpecFuncs)
while True:
	print SpecFuncs
	run_for_cfg(cfg, visited_funcs)
	run_for_cfg(cfgfast, visited_funcs)
	if CountreachedFuncs == 0 or len(SpecFuncs) == CountreachedFuncs:
		break
	CountreachedFuncs = len(SpecFuncs)
output_lines_file(inputFile, FunctionLines)
end_time = time.time()
print_time(sys.stderr)
print_time(sys.stdout)

